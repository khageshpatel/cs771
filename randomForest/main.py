import numpy as np
import sys
import sklearn as sk
from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import train_test_split
from sklearn.metrics import make_scorer
from sklearn.grid_search import GridSearchCV
from sklearn import preprocessing

trainX = []
trainY = []
test  = []
casualTarget = []
registeredTarget = []  
input_size = 0
target_size = 0
hidden_size = 8
min_maxTarget = 0
min_maxCasual = 0
min_maxRegis = 0

def genData():
	global trainX
	global trainY
	global test
	global input_size
	global target_size
	global casualTarget
	global registeredTarget
	global min_maxTarget
	global min_maxCasual
	global min_maxRegis
	train = np.loadtxt( 'train.csv', delimiter = ',' , skiprows = 1)
	test  = np.loadtxt( 'test.csv', delimiter = ','  , skiprows = 1)
	trainX = train[:,0:-3]
	trainX = np.hstack((trainX[:,0:3],(trainX[:,3] == 1).reshape( -1, 1 ),(trainX[:,3] == 2).reshape( -1, 1 ),(trainX[:,3] == 3).reshape( -1, 1 ),(trainX[:,3] == 4).reshape( -1, 1 ), trainX[:,4:]))
	trainX = np.hstack((trainX[:,0:9],(trainX[:,9] == 1).reshape( -1, 1 ),(trainX[:,9] == 2).reshape( -1, 1 ),(trainX[:,9] == 3).reshape( -1, 1 ),(trainX[:,9] == 4).reshape( -1, 1 ), trainX[:,10:]))
	hour = np.empty([trainX.shape[0],0])
	weekday = np.empty([trainX.shape[0],0])
	month = np.empty([trainX.shape[0],0])
	for i in np.unique(trainX[:,0]):
		hour = np.hstack((hour,(trainX[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(trainX[:,1]):
		weekday = np.hstack((weekday,(trainX[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(trainX[:,2]):
		month = np.hstack((month,(trainX[:,0] == i).reshape( -1, 1 )))
	trainX = np.hstack((hour,weekday,month, trainX[:,3:]))
	test = np.hstack((test[:,0:3],(test[:,3] == 1).reshape( -1, 1 ),(test[:,3] == 2).reshape( -1, 1 ),(test[:,3] == 3).reshape( -1, 1 ),(test[:,3] == 4).reshape( -1, 1 ), test[:,4:]))
	test = np.hstack((test[:,0:9],(test[:,9] == 1).reshape( -1, 1 ),(test[:,9] == 2).reshape( -1, 1 ),(test[:,9] == 3).reshape( -1, 1 ),(test[:,9] == 4).reshape( -1, 1 ), test[:,10:]))
	hour = np.empty([test.shape[0],0])
	weekday = np.empty([test.shape[0],0])
	month = np.empty([test.shape[0],0])
	for i in np.unique(test[:,0]):
		hour = np.hstack((hour,(test[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(test[:,1]):
		weekday = np.hstack((weekday,(test[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(test[:,2]):
		month = np.hstack((month,(test[:,0] == i).reshape( -1, 1 )))
	test = np.hstack((hour,weekday,month, test[:,3:]))
	casualTarget = train[:,-3]
	registeredTarget = train[:,-2]
	trainY = train[:,-1]
	input_size = trainX.shape[1]
	#min_maxTarget = preprocessing.MinMaxScaler()
	#trainY = min_maxTarget.fit_transform(trainY)
	#min_maxCasual = preprocessing.MinMaxScaler()
	#casualTarget = min_maxCasual.fit_transform(casualTarget)
	#min_maxRegis = preprocessing.MinMaxScaler()
	#registeredTarget = min_maxRegis.fit_transform(registeredTarget)
	#min_max_scaler = preprocessing.MinMaxScaler()
	#trainX = min_max_scaler.fit_transform(trainX)
	#test = min_max_scaler.transform(test)

def rmsele(actual, pred):
    squared_errors = (np.log(pred + 1) - np.log(actual + 1)) ** 2
    mean_squared = np.sum(squared_errors) / len(squared_errors)
    return np.sqrt(mean_squared)

def testData(rf1,rf2):
	p1 = rf1.predict(test)
	p2 = rf2.predict(test)
	p = p1 + p2
	i=0
	j=0
	fp = open('submit.csv','w')
	with open('../test.csv','r') as f:
		for x in f:
			if i==0:
				fp.write('datetime,count\n')
				i = 1
			else:
				xS = x.split(',')
				x = xS[0] + ',' + str(p[j]) + '\n'
				fp.write(x)
				j= j+1

def main():
	genData()
	rmsele_scorer = make_scorer(rmsele, greater_is_better=False)
	tuned_parameters = [{'max_features': ['sqrt', 'log2', 'auto'], 'max_depth': [5, 8, 12], 'min_samples_leaf': [2, 5, 10]}]
	#tuned_parameters = [{'max_features': ['sqrt'], 'max_depth': [5], 'min_samples_leaf': [2]}]
	rf1 =  GridSearchCV(RandomForestRegressor(n_jobs=1, n_estimators=1000), tuned_parameters, cv=5, scoring=rmsele_scorer).fit(trainX, casualTarget)
	rf2 =  GridSearchCV(RandomForestRegressor(n_jobs=1, n_estimators=1000), tuned_parameters, cv=5, scoring=rmsele_scorer).fit(trainX, registeredTarget)
	#p1 = min_maxTarget.inverse_transform(rf.predict(trainX))
	p1 = rf1.predict(trainX)
	p2 = rf2.predict(trainX)
	p = p1 + p2
	np.savetxt( 'trainOut.csv', p, fmt = '%.6f' )
	testData(rf1,rf2)
	

	

if __name__ == "__main__":
  sys.exit(main())