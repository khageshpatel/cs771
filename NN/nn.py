import numpy as np
import sys
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure import SigmoidLayer, LinearLayer, TanhLayer
from sklearn import preprocessing

trainX = []
trainY = []
test  = []
casualTarget = []
registeredTarget = []  
input_size = 0
target_size = 0
hidden_size = 8
min_maxTarget = 0
min_maxCasual = 0
min_maxRegis = 0

def genData():
	global trainX
	global trainY
	global test
	global input_size
	global target_size
	global casualTarget
	global registeredTarget
	global min_maxTarget
	global min_maxCasual
	global min_maxRegis
	train = np.loadtxt( 'train.csv', delimiter = ',' , skiprows = 1)
	test  = np.loadtxt( 'test.csv', delimiter = ','  , skiprows = 1)
	trainX = train[:,0:-3]
	trainX = np.hstack((trainX[:,0:3],(trainX[:,3] == 1).reshape( -1, 1 ),(trainX[:,3] == 2).reshape( -1, 1 ),(trainX[:,3] == 3).reshape( -1, 1 ),(trainX[:,3] == 4).reshape( -1, 1 ), trainX[:,4:]))
	trainX = np.hstack((trainX[:,0:9],(trainX[:,9] == 1).reshape( -1, 1 ),(trainX[:,9] == 2).reshape( -1, 1 ),(trainX[:,9] == 3).reshape( -1, 1 ),(trainX[:,9] == 4).reshape( -1, 1 ), trainX[:,10:]))
	hour = np.empty([trainX.shape[0],0])
	weekday = np.empty([trainX.shape[0],0])
	month = np.empty([trainX.shape[0],0])
	for i in np.unique(trainX[:,0]):
		hour = np.hstack((hour,(trainX[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(trainX[:,1]):
		weekday = np.hstack((weekday,(trainX[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(trainX[:,2]):
		month = np.hstack((month,(trainX[:,0] == i).reshape( -1, 1 )))
	trainX = np.hstack((hour,weekday,month, trainX[:,3:]))
	test = np.hstack((test[:,0:3],(test[:,3] == 1).reshape( -1, 1 ),(test[:,3] == 2).reshape( -1, 1 ),(test[:,3] == 3).reshape( -1, 1 ),(test[:,3] == 4).reshape( -1, 1 ), test[:,4:]))
	test = np.hstack((test[:,0:9],(test[:,9] == 1).reshape( -1, 1 ),(test[:,9] == 2).reshape( -1, 1 ),(test[:,9] == 3).reshape( -1, 1 ),(test[:,9] == 4).reshape( -1, 1 ), test[:,10:]))
	hour = np.empty([test.shape[0],0])
	weekday = np.empty([test.shape[0],0])
	month = np.empty([test.shape[0],0])
	for i in np.unique(test[:,0]):
		hour = np.hstack((hour,(test[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(test[:,1]):
		weekday = np.hstack((weekday,(test[:,0] == i).reshape( -1, 1 )))
	for i in np.unique(test[:,2]):
		month = np.hstack((month,(test[:,0] == i).reshape( -1, 1 )))
	test = np.hstack((hour,weekday,month, test[:,3:]))
	casualTarget = train[:,-3]
	registeredTarget = train[:,-2]
	registeredTarget = registeredTarget.reshape( -1, 1 )
	casualTarget = casualTarget.reshape( -1, 1 )
	trainY = train[:,-1]
	trainY = trainY.reshape( -1, 1 )
	input_size = trainX.shape[1]
	target_size = trainY.shape[1]
	min_maxTarget = preprocessing.MinMaxScaler()
	trainY = min_maxTarget.fit_transform(trainY)
	min_maxCasual = preprocessing.MinMaxScaler()
	casualTarget = min_maxCasual.fit_transform(casualTarget)
	min_maxRegis = preprocessing.MinMaxScaler()
	registeredTarget = min_maxRegis.fit_transform(registeredTarget)
	min_max_scaler = preprocessing.MinMaxScaler()
	trainX = min_max_scaler.fit_transform(trainX)
	test = min_max_scaler.transform(test)



def testData(net1,net2):
	ds1 = SDS( test.shape[1], 1 )
	ds2 = SDS( test.shape[1], 1 )
	ds1.setField( 'input', test )
	ds1.setField( 'target', np.zeros( test.shape[0] ).reshape( -1, 1 ))
	ds2.setField( 'input', test )
	ds2.setField( 'target', np.zeros( test.shape[0] ).reshape( -1, 1 ))
	p1 = net1.activateOnDataset( ds1 )
	p2 = net2.activateOnDataset( ds2 )
	p1 = min_maxCasual.inverse_transform(p1)
	p2 = min_maxRegis.inverse_transform(p2)
	p = p1 + p2
	i=0
	j=0
	fp = open('submit.csv','w')
	with open('../test.csv','r') as f:
		for x in f:
			if i==0:
				fp.write('datetime,count\n')
				i = 1
			else:
				xS = x.split(',')
				x = xS[0] + ',' + str(p[j][0]) + '\n'
				fp.write(x)
				j= j+1

def main():
	genData()
	ds1 = SDS( input_size, target_size )
	ds2 = SDS( input_size, target_size )
	ds1.setField( 'input', trainX )
	ds1.setField( 'target', casualTarget )
	ds2.setField( 'input', trainX )
	ds2.setField( 'target', registeredTarget )
	#net = buildNetwork( input_size, hidden_size, target_size, bias = True, hiddenclass = SigmoidLayer,outclass = LinearLayer)
	net1 = buildNetwork( input_size, hidden_size, target_size, bias = True, hiddenclass = TanhLayer,outclass = SigmoidLayer)
	net2 = buildNetwork( input_size, hidden_size, target_size, bias = True, hiddenclass = TanhLayer,outclass = SigmoidLayer)
	trainer1 = BackpropTrainer( net1,ds1, learningrate = 0.001, momentum = 0.99 )
	trainer2 = BackpropTrainer( net2,ds2, learningrate = 0.001, momentum = 0.99 )
	trainer1.trainUntilConvergence( verbose = True, validationProportion = 0.10, maxEpochs = 1000, continueEpochs = 10 )
	trainer2.trainUntilConvergence( verbose = True, validationProportion = 0.10, maxEpochs = 1000, continueEpochs = 10 )
	testData(net1,net2)
	p1 = net1.activateOnDataset( ds1 )
	p2 = net2.activateOnDataset( ds2 )
	p1 = min_maxCasual.inverse_transform(p1)
	p2 = min_maxRegis.inverse_transform(p2)
	p = p1 + p2
	np.savetxt( 'trainOut.csv', p, fmt = '%.6f' )

	

if __name__ == "__main__":
  sys.exit(main())