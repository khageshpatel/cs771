import numpy as np
import sys
from datetime import datetime, date, time

def main():
	i=0
	fp = open('train.csv','w')
	with open('../train.csv','r') as f:
		for x in f:
			if i==0:
				xS = x.split(',')
				xS[0] = "month"
				x = ",".join(xS)
				x = ",".join(("hour","weekday",x))
				fp.write(x)
				i = 1
			else:
				xS = x.split(',')
				hour = str(datetime.strptime(xS[0], "%Y-%m-%d %H:%M:%S").hour)
				weekday = str(datetime.strptime(xS[0], "%Y-%m-%d %H:%M:%S").weekday())
				month = str(datetime.strptime(xS[0], "%Y-%m-%d %H:%M:%S").month)
				xS[0] = month
				x = ",".join(xS)
				x = ",".join((hour,weekday,x))
				fp.write(x)
	i=0
	fp = open('test.csv','w')
	with open('../test.csv','r') as f:
		for x in f:
			if i==0:
				xS = x.split(',')
				xS[0] = "month"
				x = ",".join(xS)
				x = ",".join(("hour","weekday",x))
				fp.write(x)
				i = 1
			else:
				xS = x.split(',')
				hour = str(datetime.strptime(xS[0], "%Y-%m-%d %H:%M:%S").hour)
				weekday = str(datetime.strptime(xS[0], "%Y-%m-%d %H:%M:%S").weekday())
				month = str(datetime.strptime(xS[0], "%Y-%m-%d %H:%M:%S").month)
				xS[0] = month
				x = ",".join(xS)
				x = ",".join((hour,weekday,x))
				fp.write(x)
	

if __name__ == "__main__":
  sys.exit(main())