import numpy as np
import sys

def main():
	i=0
	fp = open('submitFilter.csv','w')
	with open('submit.csv','r') as f:
		for x in f:
			if i==0:
				fp.write('datetime,count\n')
				i = 1
			else:
				xS = x.split(',')
				xS[1] = float(xS[1])
				if xS[1] < 0:
					xS[1] = 0
				x = xS[0] + ',' + str(xS[1]) + '\n'
				fp.write(x)
	

if __name__ == "__main__":
  sys.exit(main())